<?php
/**
 * Front page
 *
 * Front page for proin website
 *
 * @package Proin
 * @subpackage Proin
 * @since Proin 1.0
 *
 * TODO: Able data to be edited on wordpress admin panel
 */

get_header(); ?>


<div class="container">
  <div class="row">
  <div class="spacer"></div>
    <div class="col-md-5">
      <p class="lead">
        <img class="img-responsive center-block" src="<?= get_template_directory_uri();?>/img/logo.png" style="height: 150px;">
      </p>
      <h3 align="center"><strong>Programadores e Informaticos Chile</strong></h3>
      <p class="lead text-justify">
        Somos una comunidad amante de la tecnología; nos gusta compartir, aprender, enseñar y disfrutar. </p>
      </p>
       <p class="lead text-justify">
       Participamos en multiples eventos y meetUps de tecnologias o simplemente nos reunimos a tener un asado y compartir.
       </p>
    </div>
    <div class="col-md-7">
    <script async class="speakerdeck-embed" data-id="4299cf7d09a4467fa69aed5d5fa9e810" data-ratio="1.77777777777778" src="//speakerdeck.com/assets/embed.js"></script>
    </div>
  </div>
</div>
<br/>

<div class="spacer"></div>
  <div class="parallax-window white-back"  data-parallax="scroll" data-image-src="https://scontent-lax3-1.xx.fbcdn.net/hphotos-xlt1/t31.0-8/12615487_10205435002336812_2708854201665486228_o.jpg">
    <div class="container">
      <h2> Nuestros partners </h2>
      <div id="owl-demo">
        <div class="item"><img src="<?= get_template_directory_uri();?>/img/gitlab.png" alt="gitLab"></div>
        <div class="item"><img src="<?= get_template_directory_uri();?>/img/uss.png" alt="Universidad San Sebastian"></div>
        <div class="item"><img src="<?= get_template_directory_uri();?>/img/hackingcl.png" alt="hacking chile"></div>
        <div class="item"><img src="<?= get_template_directory_uri();?>/img/betabeer.jpg" alt="beta beers"></div>
        <div class="item"><img src="<?= get_template_directory_uri();?>/img/noders.png" alt="Noders"></div>
      </div>
    </div>
  </div>


<div class="container">
  <h2>Nuestro Equipo</h2>
  <br/>
  <div class="row text-center">
    <div class="col-lg-4">
    <img class="img-circle" src="<?= get_template_directory_uri();?>/img/ricardo_castillo.jpg" alt="ricardo castillo" width="140" height="140">
      <h2>Ricardo Castillo</h2>
      <p>Backend Developer principalmente en JS y PHP.</p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
    <img class="img-circle" src="<?= get_template_directory_uri();?>/img/kevin_zelada.jpg" alt="kevin zelada" width="140" height="140">
      <h2>Kevin Zelada Ruff</h2>
      <p>Developer & Emprendedor Tecnológico. </p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
    <img class="img-circle" src="<?= get_template_directory_uri();?>/img/mauricio_lineros.jpg" alt="mauricio lineros" width="140" height="140">
      <h2>Mauricio Lineros</h2>
      <p>Soy un apasionado de la tecnología, la educación y el crecimiento personal.</p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
    <img class="img-circle" src="<?= get_template_directory_uri();?>/img/luis_jury.jpg" alt="mauricio lineros" width="140" height="140">
      <h2>Luis Jury</h2>
      <p>Ingeniero de software proactivo y versátil, apasionado por aprender nuevas tecnologías</p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
    <img class="img-circle" src="<?= get_template_directory_uri();?>/img/ernesto_acevedo.jpg" alt="mauricio lineros" width="140" height="140">
      <h2>Ernesto Acevedo</h2>
      <p>Ruby on Rails Developer en Innovandio</p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
    <img class="img-circle" src="<?= get_template_directory_uri();?>/img/elvis_romero.jpg" alt="elvis romero" width="140" height="140">
      <h2>Elvis Romero</h2>
      <p>Analista IT en FOLLOW UP COMUNICACION. Estudiante de Tercer año de la carrera de Ingeniería en Informática.</p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4 col-lg-offset-4">
    <img class="img-circle" src="<?= get_template_directory_uri();?>/img/unknown.jpg" alt="siguiente miembro" width="140" height="140">
      <h2>¡Este podrias ser tu!. </h2>
      <p>Escribenos por nuestras redes sociales. Siempre necesitamos tu apoyo.</p>
    </div><!-- /.col-lg-4 -->
  </div> <!-- /.row -->
  <div class="spacer"></div>
  <div class="spacer"></div>

  <div class="row">
    <div class="col-md-6">
      <br/>
      <canvas id="myChart" class="img-responsive center-block"></canvas>
    </div>
    <div class="col-md-6">
      <h2>Nuestros Miembros </h2>
      <p class="lead">Somos una comunidad en constante crecimiento.</p>
      <p class="lead">En Proin Chile crecemos constantemente en todas nuestras redes, donde nuestra comunidad comparte desde conocimiento hasta gratas platicas. Acá se puede ver el crecimiento mes a mes.</p>
    </div>
  </div>
</div>


<?php
get_footer();
